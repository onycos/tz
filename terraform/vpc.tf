resource "aws_vpc" "tz_vpc" {
  cidr_block = var.cidr_block_vpc

  tags = {
    Name = "tz_vpc"
  }
}

resource "aws_subnet" "database_subnet" {
  vpc_id            = aws_vpc.tz_vpc.id
  cidr_block        = var.cidr_block_sg_db 
  availability_zone = var.availability_zone_db

  tags = {
    Name = "database_subnet"
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.tz_vpc.id
  cidr_block              = var.cidr_block_sg_public
  availability_zone       = var.availability_zone_public
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.tz_vpc.id
  cidr_block        = var.cidr_block_sg_private
  availability_zone = var.availability_zone_private

  tags = {
    Name = "private_subnet"
  }
}

resource "aws_internet_gateway" "tz_gateway" {
  vpc_id = aws_vpc.tz_vpc.id

  tags = {
    Name = "tz_gateway"
  }
}

resource "aws_nat_gateway" "tz_nat_gateway" {
  allocation_id = aws_eip.my_eip.id
  subnet_id     = aws_subnet.public_subnet.id
}

resource "aws_eip" "my_eip" {
  vpc = true
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.tz_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tz_gateway.id
  }
}

resource "aws_route_table_association" "public_subnet_association" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.tz_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.tz_nat_gateway.id
  }
}

resource "aws_route_table_association" "private_subnet_association" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_route_table.id
}
