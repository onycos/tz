resource "aws_key_pair" "ubuntu" {
  key_name   = "tg-key"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFXfSKIcbpjvf+w7TXhLmQdnnF8MYUctScNl9pvk49yG root@endru"
}

resource "aws_instance" "unifi-controller" {
  ami                    = var.ami
  instance_type          = var.instance_type           
  subnet_id              = aws_subnet.public_subnet.id
  key_name               = aws_key_pair.ubuntu.key_name  
  vpc_security_group_ids = [aws_security_group.unifi-controller.id]
  tags = {
    Name = "UniFi Controller"
  }
  
}

data "aws_eip" "ip" {
  depends_on = [aws_instance.unifi-controller]
}


output "public_ip" {
  value = aws_instance.unifi-controller.public_ip
}

resource "aws_security_group" "unifi-controller" {
  vpc_id = aws_vpc.tz_vpc.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3478
    to_port     = 3478
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 6789
    to_port     = 6789
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}