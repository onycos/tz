resource "aws_ecr_repository" "atreyus" {
  name                 = "${var.project_name}"
  image_tag_mutability = "MUTABLE"
}

