Для запуску інфраструктури потрібно виконати команди в директорії terraform
terraform init
terraform apply
Для конфігурації ec2 інстанса в директорії ansible змінюємо ip адрес в файлі inventory.ini та виконуємо команду
ansible-playbook deploy.yml -i inventory.ini --user=ubuntu

Перед всіма діями потрібно локально залогінитися на своїй машині в aws cli та під своїм користувачем запускати terraform та ansible
ansible встановлює потрібні пакети та запускає docker-compose також налаштовує cron для надсилання бекапів на s3
terraform створює vps, sg, ecr, ec2 для проекта, також прокидаю ssh key

docker-compose створює контейнер  unifi-controller, та міні систему моніторингу