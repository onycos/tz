FROM ubuntu:20.04

RUN sudo apt-get update && sudo apt-get install ca-certificates apt-transport-https

RUN echo 'deb [ arch=amd64,arm64 ] https://www.ui.com/downloads/unifi/debian stable ubiquiti' | tee /etc/apt/sources.list.d/100-ubnt-unifi.list

RUN wget -O /etc/apt/trusted.gpg.d/unifi-repo.gpg https://dl.ui.com/unifi/unifi-repo.gpg

RUN apt-get update

RUN apt-get install -y unifi

EXPOSE 8080 8443 8880 8843 6789

CMD ["/usr/bin/java", "-jar", "/usr/lib/unifi/lib/ace.jar", "start"]
